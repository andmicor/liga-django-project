from django.contrib import admin
from gestion.models import Equipo, Jugador, Arbitro, Resultados

# Register your models here.


admin.site.register(Equipo)
admin.site.register(Jugador)
admin.site.register(Arbitro)
admin.site.register(Resultados)