from django.forms.extras.widgets import SelectDateWidget
from django.forms import ModelForm
from django import forms
from gestion.models import Equipo, Jugador, Resultados, Arbitro


class EquipoForm(ModelForm):
    class Meta:
        model = Equipo
        
        fields = '__all__'


class JugadorForm(ModelForm):
    class Meta:
        model = Jugador
        
        fields = '__all__'


class ResultadosForm(ModelForm):
    class Meta:
        model = Resultados
        
        fields = '__all__'

class ArbitroForm(ModelForm):
    class Meta:
        model = Arbitro
        
        fields = '__all__'

