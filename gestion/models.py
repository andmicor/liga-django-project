from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
# Create your models here.


@python_2_unicode_compatible 
class Equipo(models.Model):
	nombre = models.CharField(max_length=200)
	ciudad = models.CharField(max_length=200)
	anyo_creacion= models.IntegerField()
	historia=models.TextField()
	entrenador= models.CharField(max_length=200)
	puntos=models.IntegerField()
	goles_favor=models.IntegerField()
	goles_contra=models.IntegerField()



	def __str__(self):
		return self.nombre

@python_2_unicode_compatible 
class Jugador(models.Model):
	equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name="equipo")
	nombre=models.CharField(max_length=200)
	lugar_nacimiento=models.CharField(max_length=200)
	fecha_nacimiento=models.DateTimeField(default='')
	edad=models.IntegerField()
	posicion=models.CharField(max_length=200)
	historial=models.TextField()
	goles=models.IntegerField()
	equipos_anteriores=models.ManyToManyField(Equipo, related_name='equipos_anteriores')

	def __str__(self):
		return self.nombre


@python_2_unicode_compatible 
class Arbitro(models.Model):
	nombre=models.CharField(max_length=200)
	colegio=models.CharField(max_length=200)

	def __str__(self):
		return self.nombre

class Resultados(models.Model):
	arbitro=models.ForeignKey(Arbitro, on_delete=models.CASCADE, related_name="arbitro")
	equipo_1=models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name="equipo_1")
	equipo_2=models.ForeignKey(Equipo, on_delete=models.CASCADE, related_name="equipo_2")
	goles_1=models.IntegerField()
	goles_2=models.IntegerField()
	jornada=models.IntegerField()



