from django.conf.urls import url
from django.core.urlresolvers import reverse
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from gestion.views import Calendario, Estadisticas, Arbitros
from . import views


urlpatterns = [

# Lists
   url(r'^$', views.index, name='index'),
   url(r'^equipos/$', views.equipos, name='equipos'),
   url(r'^jugadores/$', views.jugadores, name='jugadores'),
   url(r'^resultados/$', views.resultados, name='resultados'),
   url(r'^calendario/$', Calendario.as_view(), name='calendario'),
   url(r'^estadisticas/$', Estadisticas.as_view(), name='estadisticas'),
   url(r'^clasificacion/$', views.clasificacion, name='clasificacion'),
   url(r'^arbitros/$', Arbitros.as_view(), name='arbitros'),




 # Details
   url(r'^jugadores/(?P<jugador_id>\d+)/$',views.detail_jugador,name='detail_jugador'),
   url(r'^equipos/(?P<equipo_id>\d+)/$',views.detail_equipo,name='detail_equipo'),


#Add
   url(r'^equipo/new/$', views.equipo_new, name='equipo_new'),
   url(r'^jugador/new/$', views.jugador_new, name='jugador_new'),
   url(r'^resultado/new/$', views.resultado_new, name='resultado_new'),
   url(r'^arbitro/new/$', views.arbitro_new, name='arbitro_new'),


   # Edit
   url(r'^equipo/edit/(?P<equipo_id>\d+)/$', views.equipo_edit, name = 'equipo_edit'),
   url(r'^jugador/edit/(?P<jugador_id>\d+)/$', views.jugador_edit, name = 'jugador_edit'),  
   url(r'^resultado/edit/(?P<resultado_id>\d+)/$', views.resultado_edit, name = 'resultado_edit'), 
   url(r'^arbitro/edit/(?P<arbitro_id>\d+)/$', views.arbitro_edit, name = 'arbitro_edit'),

   #Delete
   url(r'^equipo/delete/(?P<equipo_id>\d+)/$', views.equipo_delete, name='equipo_delete'),
   url(r'^jugador/delete/(?P<jugador_id>\d+)/$', views.jugador_delete, name='jugador_delete'),

]+ static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)