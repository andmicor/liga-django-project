from django.shortcuts import render,  get_object_or_404, redirect, render_to_response
from django.http import HttpResponse
from django.http import Http404
from gestion.models import Equipo, Jugador, Arbitro, Resultados
from django.core.urlresolvers import reverse_lazy
from gestion.forms import EquipoForm, JugadorForm, ResultadosForm, ArbitroForm
from django.contrib.auth.decorators import login_required
from django.views.generic.base import View
from django.conf import settings
from django.utils.decorators import method_decorator

# Create your views here.

def index(request):
	return render(request, 'gestion/index.html')


def equipos(request):
	equipos_list = Equipo.objects.order_by('anyo_creacion')
	context = {'equipos_list': equipos_list}
	return render(request, 'gestion/equipos.html', context)


def detail_equipo(request, equipo_id):
	equipo = get_object_or_404(Equipo, pk=equipo_id)
	return render(request, 'gestion/detail_equipo.html', {'equipo': equipo})	

def jugadores(request):
	jugadores_list = Jugador.objects.all()
	context = {'jugadores_list': jugadores_list}
	return render(request, 'gestion/jugadores.html', context)	

def detail_jugador(request, jugador_id):
	jugador = get_object_or_404(Jugador, pk=jugador_id)
	return render(request, 'gestion/detail_jugador.html', {'jugador': jugador})		

def clasificacion(request):
    equipos = Equipo.objects.all().order_by("-puntos")
    resultados = Resultados.objects.all()
    for equipo in equipos:
        for resultado in resultados:
            if resultado.equipo_1==equipo:
                equipo.goles_favor += resultado.goles_1
                equipo.goles_contra += resultado.goles_2
                if resultado.goles_1 > resultado.goles_2:
                    equipo.puntos += 3
                if resultado.goles_2 == resultado.goles_1:
                    equipo.puntos += 1
            if resultado.equipo_2==equipo:
                equipo.goles_favor += resultado.goles_2
                equipo.goles_contra += resultado.goles_1
                if resultado.goles_1 < resultado.goles_2:
                    equipo.puntos += 3
                if resultado.goles_2 == resultado.goles_1:
                    equipo.puntos += 1
        l_equipos = list(equipos)
        l_equipos.sort(key=lambda x: (x.puntos, x.goles_favor-x.goles_contra, x.goles_favor), reverse=True)
        


    return render_to_response("gestion/clasificacion.html",dict(equipos = l_equipos, resultados = resultados, user = request.user))

def resultados(request):
    resultados_list = Resultados.objects.order_by('jornada')
    context = {'resultados_list': resultados_list}
    return render(request, 'gestion/resultados.html', context)   
#Add a Equipo
@login_required(login_url = '/users/login')
def equipo_new(request):
	if request.method == 'POST':
		form = EquipoForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/gestion/')
	else:
		form = EquipoForm()
	context = {'form':form}
    
	return render(request, 'gestion/equipo_new.html', context)

 #Add a Jugador 
@login_required(login_url = '/users/login')  
def jugador_new(request):
	if request.method == 'POST':
		form = JugadorForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/gestion/')
	else:
		form = JugadorForm()
	context = {'form':form}
    
	return render(request, 'gestion/jugador_new.html', context)


#Edit a Equipo
@login_required(login_url = '/users/login')
def equipo_edit(request, equipo_id):
    equipo = get_object_or_404(Equipo, pk = equipo_id)
    if request.method == 'POST':
        form = EquipoForm(request.POST, request.FILES, instance = equipo)
        if form.is_valid():
            form.save()
            return redirect('/gestion/')
    else:
        form = EquipoForm(instance = equipo)
    context = {'form':form}
    
    return render(request, 'gestion/equipo_new.html', context)

#Delete a equipo
@login_required(login_url = '/users/login')
def equipo_delete(request, equipo_id):
    """
    Returns specific bottle
    """
    equipo = get_object_or_404(Equipo, pk = equipo_id)
    
    # Only staff can delete bottle
    if request.user.is_staff:       
        equipo.delete()
    else:
        message = "You have no permissions for deleting a equipo"
        context = {'message': message}
        return render(request, "gestion/error_message.html", context)
    
    return redirect('/gestion/') 

#Edit a Jugador
@login_required(login_url = '/users/login')
def jugador_edit(request, jugador_id):
    jugador = get_object_or_404(Jugador, pk = jugador_id)
    if request.method == 'POST':
        form = JugadorForm(request.POST, request.FILES, instance = jugador)
        if form.is_valid():
            form.save()
            return redirect('/gestion/')
    else:
        form = JugadorForm(instance = jugador)
    context = {'form':form}
    
    return render(request, 'gestion/jugador_new.html', context)

    #Delete a jugador
@login_required(login_url = '/users/login')
def jugador_delete(request, jugador_id):
    """
    Returns specific bottle
    """
    jugador = get_object_or_404(Jugador, pk = jugador_id)
    
    # Only staff can delete bottle
    if request.user.is_staff:       
        jugador.delete()
    else:
        message = "You have no permissions for deleting a jugador"
        context = {'message': message}
        return render(request, "gestion/error_message.html", context)
    
    return redirect('/gestion/')

#Add a Resultado
@login_required(login_url = '/users/login')
def resultado_new(request):
	if request.method == 'POST':
		form = ResultadosForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/gestion/')
	else:
		form = ResultadosForm()
	context = {'form':form}
    
	return render(request, 'gestion/resultado_new.html', context)    

#Edit a Resultado
@login_required(login_url = '/users/login')
def resultado_edit(request, equipo_id):
    resultado = get_object_or_404(Resultados, pk = resultado_id)
    if request.method == 'POST':
        form = ResultadosForm(request.POST, request.FILES, instance = resultado)
        if form.is_valid():
            form.save()
            return redirect('/gestion/')
    else:
        form = ResultadosForm(instance = resultado)
    context = {'form':form}
    
    return render(request, 'gestion/resultado_new.html', context)

#Add a Arbitro
@login_required(login_url = '/users/login')
def arbitro_new(request):
	if request.method == 'POST':
		form = ArbitroForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/gestion/')
	else:
		form = ArbitroForm()
	context = {'form':form}
    
	return render(request, 'gestion/arbitro_new.html', context)    

#Edit a Arbitro
@login_required(login_url = '/users/login')
def arbitro_edit(request, equipo_id):
    arbitro = get_object_or_404(Arbitro, pk = arbitro_id)
    if request.method == 'POST':
        form = ArbitroForm(request.POST, request.FILES, instance = arbitro)
        if form.is_valid():
            form.save()
            return redirect('/gestion/')
    else:
        form = ResultadosForm(instance = arbitro)
    context = {'form':form}
    
    return render(request, 'gestion/arbitro_new.html', context)


#Mostrar el calendario con Vistas basadas en clases
class Calendario(View):
    template_name = 'gestion/calendario.html'
    
    def get(self, request, *args, **kwargs):
        list_equipos = Equipo.objects.all()
        return render(request, self.template_name, {'list_equipos':list_equipos})

#Mostrar las estadisticas con Vistas basadas en clases
class Estadisticas(View):
    template_name = 'gestion/estadisticas.html'
    
    def get(self, request, *args, **kwargs):
        list_jugadores = Jugador.objects.order_by('-goles')
        return render(request, self.template_name, {'list_jugadores':list_jugadores})



#Mostrar las estadisticas con Vistas basadas en clases
class Arbitros(View):
    template_name = 'gestion/arbitros.html'
    
    def get(self, request, *args, **kwargs):
        arbitros = Arbitro.objects.all()
        return render(request, self.template_name, {'arbitros':arbitros})